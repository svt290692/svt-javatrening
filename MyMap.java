import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeMap;


public class MyMap<K,T>{
	Wrapper< Pair<K,T> > root;
	Comparator<K> mComp;
	
	MyMap(Comparator<K> Comp){
		this.mComp = Comp;
	}
	boolean add(K key,T obj){
		if(root == null)
			root = new Wrapper< Pair<K,T> >(new Pair<K,T>(key,obj), null, null);
		else
			addTo(root,key,obj);
		
		return true;
	}
	ArrayList<Pair<K,T> > toArrayList(){
		if(root == null)return null;
		ArrayList<Pair<K,T> > arr = new ArrayList<Pair<K,T> >();
		addToArrayList(arr, root);
		return arr;
	}
	private void addToArrayList(ArrayList<Pair<K,T> > arr,Wrapper<Pair<K,T> > w){
		if(w == null)return;
		arr.add(w.obj);
		addToArrayList(arr, w.left);
		addToArrayList(arr, w.right);
	}
	private void addTo(Wrapper< Pair<K,T> > w,K key, T obj){
		if(mComp.compare(w.obj.key,key) <= 0){
			if(w.left == null)
				w.left = new Wrapper< Pair<K,T> >(new Pair<K,T>(key,obj), null, null);
			else
			addTo(w.left,key,obj);
		}
		else{
			if(w.right == null)
				w.right = new Wrapper< Pair<K,T> >(new Pair<K,T>(key,obj), null, null);
			else
				addTo(w.right,key,obj);
		}
	}
	
	static class Wrapper<T>{
		public Wrapper(T obj, Wrapper<T> left, Wrapper<T> right) {
			super();
			this.obj = obj;
			this.left = left;
			this.right = right;
		}
		
		T obj;
		Wrapper<T> left;
		Wrapper<T> right;
	}
	static class Pair<K,T>{
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "Pair [key=" + key + ", value=" + value + "]";
		}
		public Pair(K key, T value) {
			this.key = key;
			this.value = value;
		}
		K key;
		T value;
	}
	public static void main(String args[]){
		MyMap<String,Integer> app = new MyMap<String,Integer>(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}});
		app.add("REsadasRE",1);
		app.add("Привет",2);
		app.add("asdasdривет1",3);
		app.add("Привет2",4);
		for(Pair<String, Integer> p : app.toArrayList()){
			System.out.println(p);
		}
	}
}
