package ru.main;

import java.util.Iterator;

public class MyLinkedList<T> implements Iterable<T> {
	
	Wrapper<T> firstElm;
	Wrapper<T> endElm;
	int countOfElm;
	
	public MyLinkedList() {
		super();
		countOfElm = 0;
		firstElm = null;
		endElm = null;
	}
	
	public void pushBack(T val){
		if(countOfElm == 0){
			countOfElm++;
			firstElm = new Wrapper(null,val,null);
			endElm = firstElm;
		}
		else{
			endElm.next = new Wrapper(endElm,val,null);
			endElm = endElm.next;
		}
	}
	
	@Override
	public Iterator<T> iterator() {
		
		return new MyItertor();
	}
	
	private class MyItertor implements Iterator<T> {

		public MyItertor() {
			this.curElm = firstElm;
		}

		Wrapper curElm;
		@Override
		public boolean hasNext() {
			if(curElm == null)
				return false;
			else
				return true;
		}

		@Override
		public T next() {
			T current = (T) curElm.m_obj;
			curElm = curElm.next;
			return current;
			
		}

		@Override
		public void remove() {
			if(countOfElm == 0 || curElm.prev == null)
				return;
			curElm.prev.next = curElm.next;
		}
		
	}
	
	static class Wrapper<T>{
		public Wrapper(Wrapper back, T m_obj, Wrapper front) {
			prev = back;
			this.m_obj = m_obj;
			next = front;
		}

		Wrapper prev;
		
		T m_obj;
		
		Wrapper next;
	}
}
