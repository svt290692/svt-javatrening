package ru.Conditions;

public class ConditionFirstCharTopRegister implements Condition {

	@Override
	public boolean isSatisfy(String s) {
		if(!s.isEmpty())
		return Character.isUpperCase(s.charAt(0));
		else return false;
	}

}
