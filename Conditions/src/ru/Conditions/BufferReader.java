package ru.Conditions;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BufferReader implements Runnable {

	
	public BufferReader(String fileTowrite, WordsBuffer buffer, Condition...  condotions) {
		super();
		FileTowrite = fileTowrite;
		Buffer = buffer;
		
		try {
			writer = new BufferedWriter(
					new FileWriter(FileTowrite));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for( Condition c : condotions){
			this.conditions.add(c);
		}
	}

	public BufferReader(String fileTowrite, WordsBuffer buffer) {
		super();
		FileTowrite = fileTowrite;
		Buffer = buffer;
		try {
			writer = new BufferedWriter(
					new FileWriter(FileTowrite));
		} catch (IOException e) {
			// TODO some thing
			e.printStackTrace();
		}
	}
	
	public void addCondition(Condition c){
		conditions.add(c);
	}
	
	private final String FileTowrite;
	private final WordsBuffer Buffer;
	private BufferedWriter writer = null;
	
	private boolean Stop;
	private List<Condition> conditions = new ArrayList<Condition>();
	
	@Override
	public void run() {
		String word = null;
		while(!Stop){
			try{
			word = Buffer.pop();
			}catch(InterruptedException e){
				try {
					writer.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				break;
			}
			try {
				Condition cond = valid(word);
				if(cond != null){
					String result = "Word: " + "<" + word + ">" +
							"  " + "time:" + " <" + Calendar.getInstance().getTime() + ">" + "Condition: " +
							cond + "\n";
				System.out.println(result);
					writer.write(result);
				}	
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	public void SetStop(){
		Stop = true;
	}
	private Condition valid(String Word){
		for(Condition c : conditions){
			if(c.isSatisfy(Word)) return c;
		}
		return null;
	}
}
