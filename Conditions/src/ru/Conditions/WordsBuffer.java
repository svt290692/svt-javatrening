package ru.Conditions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class WordsBuffer {
	
	final LinkedBlockingQueue<String> MyBuffer = new LinkedBlockingQueue<String>(1024);
	
//	ReentrantLock lock = new ReentrantLock();
//	Condition notFull = lock.newCondition(); 
//	Condition notEmpty = lock.newCondition();
	
	public WordsBuffer() {

	}
//	public int getMaxSize(){
//		return MyBuffer.r;
//	}
	public void push(String s){
//		lock.lock();
			
//		while(MyBuffer.size() >= MaxSize){
//		
//				try {
//					notFull.await();
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
//			
//		}
		try {
			MyBuffer.put(s);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		notEmpty.signal();
//		lock.unlock();
	}
	public  String pop() throws InterruptedException{
//		lock.lock();
//			
//		while(MyBuffer.size() == 0){
//			
//				try {
//					notEmpty.await();
//				} catch (InterruptedException e) {
//					throw e;
//				}
//			
//			
//		}
//		String cur = MyBuffer.get(0);
//		MyBuffer.remove(0);
//		notFull.signal();
//		lock.unlock();
		try {
		return MyBuffer.take();
		} catch (InterruptedException e) {
			throw e;
		}
	}
}
