package ru.Conditions;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JOptionPane;

public class BufferWriter implements Runnable {


	private final WordsBuffer Buffer;
	private final String FileaName;
	private boolean Stop = false;
	
	
	public BufferWriter(WordsBuffer buffer, String fileaName) {
		super();
		Buffer = buffer;
		FileaName = fileaName;
	}


	
	public void run() {
		FileReader fr = null;
		try {
			 fr = new FileReader(FileaName);
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(),"File not Found",JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		String line = null;
		BufferedReader br = new BufferedReader(fr);
		while(!Stop){
			try {
				if(Thread.currentThread().interrupted()){
					Stop = true;
					break;
				}
				line = br.readLine();
				
				if(line == null){
					Stop = true;
					fr.close();
					break;
				}
				
			}catch (IOException e) {
				JOptionPane.showMessageDialog(null, e.getMessage(),"File read error",JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
			
			writeToBuffer(line.split(" "));
		}
		
		try {
			fr.close();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
	private void writeToBuffer(String[] arr){
		for(String s : arr){
			Buffer.push(s);
		}
	}
	public void SetStop(){
		Stop = true;
	}
}
