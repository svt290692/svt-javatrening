package ru.Conditions;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MyProgramm {

	public static void main(String[] args) {
		WordsBuffer buffer = new WordsBuffer();
		BufferReader reader = new BufferReader("out", buffer,new CondiionWordLength(3), new CondiionWordLength(5));
		BufferWriter writer = new BufferWriter(buffer, "book1.txt");
		
		Thread readerThread = new Thread(reader);
		Thread writerThread = new Thread(writer);
		
//		ExecutorService exec = Executors.newFixedThreadPool(1);
//		exec.execute(writerThread);
//		exec.execute(readerThread);
		writerThread.start();
		readerThread.start();
		
		try {
			writerThread.join();
			reader.SetStop();
			readerThread.interrupt();
			readerThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
