package ru.Conditions;

public class ConditionWordEqualsIgnoreCase implements Condition {

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EqualsIgnoreCase [word=" + word + "]";
	}
	private final String word;
	public ConditionWordEqualsIgnoreCase(String word) {
		super();
		if(word == null) throw new IllegalArgumentException(word);
		this.word = word;
	}
	@Override
	public boolean isSatisfy(String s) {
		
		return word.equalsIgnoreCase(s);
	}

}
