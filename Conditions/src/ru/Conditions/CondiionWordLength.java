package ru.Conditions;

public class CondiionWordLength implements Condition {

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CondiionWordLength Length= " + mParam;
	}

	private final int mParam;
	
	public CondiionWordLength(int mParam) {
		super();
		this.mParam = mParam;
	}

	@Override
	public boolean isSatisfy(String s) {
		
		return s.length() == mParam;
	}

}
