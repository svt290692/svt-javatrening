import java.util.ArrayList;
import java.util.Comparator;


public class MyTree<T>{
	Wrapper<T> root;
	Comparator<T> mComp;
	
	MyTree(Comparator<T> Comp){
		this.mComp = Comp;
	}
	boolean add(T obj){
		if(root == null)
			root = new Wrapper<T>(obj, null, null);
		else
			addTo(root,obj);
		
		return true;
	}
	ArrayList<T> toArrayList(){
		if(root == null)return null;
		ArrayList<T> arr = new ArrayList<T>();
		addToArrayList(arr, root);
		return arr;
	}
	private void addToArrayList(ArrayList<T> arr,Wrapper<T> w){
		if(w == null)return;
		arr.add(w.obj);
		addToArrayList(arr, w.left);
		addToArrayList(arr, w.right);
	}
	private void addTo(Wrapper<T> w,T obj){
		if(mComp.compare(w.obj,obj) <= 0){
			if(w.left == null)
				w.left = new Wrapper<T>(obj, null, null);
			else
			addTo(w.left,obj);
		}
		else{
			if(w.right == null)
				w.right = new Wrapper<T>(obj, null, null);
			else
				addTo(w.right,obj);
		}
	}
	
	static class Wrapper<T>{
		public Wrapper(T obj, Wrapper<T> left, Wrapper<T> right) {
			super();
			this.obj = obj;
			this.left = left;
			this.right = right;
		}
		T obj;
		Wrapper<T> left;
		Wrapper<T> right;
	}
	public static void main(String args[]){
		MyTree<String> app = new MyTree(new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		});
		app.add("REsadasRE");
		app.add("Привет");
		app.add("Привет1");
		app.add("Привет2");
		for(String s : app.toArrayList()){
			System.out.println(s);
		}
	}
}
