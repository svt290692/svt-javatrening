package Students;

import java.util.Comparator;

public class StudsCOmpar implements Comparator<Student> {

	@Override
	public int compare(Student arg0, Student arg1) {
		int secName = arg0.getSecondName().compareTo(arg1.getSecondName());
		int Fname = arg0.getFirstName().compareTo(arg1.getFirstName());
		int TName = arg0.getThirdName().compareTo(arg1.getThirdName());
		if(secName != 0 )return secName;
		else if(Fname != 0)return Fname;
		else if(TName != 0 )return TName;
		
		return 0;
	}

}
