package Students;

public class StudentsGroupFilter implements StudentsFilter {
	
	public StudentsGroupFilter(String group) {
		super();
		this.group = group;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentsGroupFilter other = (StudentsGroupFilter) obj;
		if (group == null) {
			if (other.group != null)
				return false;
		} else if (!group.equals(other.group))
			return false;
		return true;
	}
	String group;
	@Override
	public boolean isValid(Student stud){
		if(stud.getGroup().equals(stud))
		return true;
		else
			return false;
	}

}
