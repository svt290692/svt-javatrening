package Students;
import java.io.Serializable;

public class Student implements Serializable,Comparable<Student> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 109876567890L;

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Student: " + FirstName + " " + SecondName
				+ " " + ThirdName +  " Birth: " + Birth 
				+ " group: " + group + " Departament: " + departament;
	}
	String FirstName;
	String SecondName;
	String ThirdName;
	String departament;
	String group;
	String Birth;
	
	Student()
	{
		FirstName = new String();
		SecondName = new String();
		ThirdName = new String();
		departament = new String();
		Birth = new String();
		group = new String();
	}
	Student(String fName,String sName,String tName,String Depart,String birth,String group)
	{
		FirstName = fName;
		SecondName = sName;
		ThirdName = tName;
		departament = Depart;
		Birth = birth;
		this.group = group;
	}
	
	public String getFirstName(){
		return FirstName;
	}
	public String getSecondName(){
		return SecondName;
	}
	public String getThirdName(){
		return ThirdName;
	}
	public String getDeportament(){
		return departament;
	}
	public String getGroup(){
		return group;
	}
	public String getBirth(){
		return Birth;
	}
//	@Override
	public int compareTo(Student arg0) {
		int secName = this.getSecondName().compareTo(arg0.getSecondName());
		int Fname = this.getFirstName().compareTo(arg0.getFirstName());
		int TName = this.getThirdName().compareTo(arg0.getThirdName());
		if(secName != 0 )return secName;
		else if(Fname != 0)return Fname;
		else if(TName != 0 )return TName;
		
		return 0;
	}
	public Student clone(){
		return new Student(
				new String(FirstName),
				new String(SecondName),
				new String(ThirdName),
				new String(departament),
				new String(Birth),
				new String(group)
				);
		
	}
}
