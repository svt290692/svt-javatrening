package Students;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class StudentsXmlBase implements StudentsDataBase {

	List<Student> mStuds;
	String BaseName;
	public StudentsXmlBase(String BaseName){
		mStuds = new ArrayList<Student>();
		this.BaseName = BaseName;
		LoadFromXml();
	}
	private void LoadFromXml(){
		try{
		SAXBuilder parser = new SAXBuilder();
		FileReader fr = new FileReader(BaseName + ".xml");
		
		Document doc = parser.build(fr);
		
		for(Element elm : doc.getRootElement().getChildren()){
			
			Student newStud = new Student(
					elm.getAttributeValue("firstName"),
					elm.getAttributeValue("surname"),
					elm.getAttributeValue("thirdName"),
					elm.getAttributeValue("Deportament"),
					elm.getAttributeValue("Birth"),
					elm.getAttributeValue("group"));
			mStuds.add(newStud);
		}} catch(FileNotFoundException e){
			JOptionPane.showMessageDialog(null,
					"Can not load base from"+BaseName+".xml" + "\n" + "error:" + e.getMessage(),
					"Error File not found", JOptionPane.ERROR_MESSAGE);
		} catch (JDOMException e) {
			JOptionPane.showMessageDialog(null,
					"Error parsing" + "\n" + "error:" + e.getMessage(),
					"Error parsing", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,
					"error read from"+BaseName+".xml" + "\n" + "error:" + e.getMessage(),
					"Error Input", JOptionPane.ERROR_MESSAGE);
		}
	}
	private void SaveFromXml(){
		Element rootElm = new Element("Students");
		Document doc = new Document(rootElm);
		
		for(Student stud : mStuds){
			Element curStud = new Element("Student");
			
			curStud.setAttribute("firstName", stud.getFirstName());
			curStud.setAttribute("surname", stud.getSecondName());
			curStud.setAttribute("thirdName", stud.getThirdName());
			curStud.setAttribute("Birth", stud.getBirth());
			curStud.setAttribute("Deportament", stud.getDeportament());
			curStud.setAttribute("group", stud.getGroup());
			
			rootElm.addContent(curStud);
		}
		
			XMLOutputter output = new XMLOutputter(Format.getPrettyFormat());
			try {
				FileWriter fw = new FileWriter(BaseName + ".xml");
				output.output(doc, fw);
				fw.close();
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null,
						"error write to "+BaseName+".xml" + "\n" + "error:" + e.getMessage(),
						"Error In/output", JOptionPane.ERROR_MESSAGE);
			}

	}
	@Override
	public void AddStudent(Student stud) {
		mStuds.add(stud);
		SaveFromXml();
	}

	@Override
	public boolean RemoveStudent(Student stud){
		boolean isFind = false;
		for(Student st : mStuds){
			if(st.equals(stud)){
				mStuds.remove(st);
				isFind = true;
				break;
			}
		}
		SaveFromXml();
		return isFind;
	}

	@Override
	public List<Student> FindStudents(String fName, String sName,
			String tName) {
		// TODO Auto-generated method stub
		return FindFromFilter(new StudentsFIOExactMathingFilter(fName, sName, tName));
	}

	@Override
	public List<Student> FindStudents(String sName) {
				return FindFromFilter(new StudentsFIOConFilter(null, sName, null));
	}

	@Override
	public List<Student> FindStudentsGroup(String group) {
				return FindFromFilter(new StudentsGroupFilter(group));
	}

	@Override
	public List<Student> FindCoincidence(String fName, String sName,
			String tName) {

		return FindFromFilter(new StudentsFIOConFilter(fName, sName, tName));
	}

	@Override
	public List<Student> FindFromFilter(StudentsFilter filter) {
		
		List<Student> finded = new ArrayList<Student>();
		for(Student st : mStuds){
			if(filter.isValid(st))
				finded.add(st);
		}
		return finded;
	}

	@Override
	public List<Student> GetAllStudents() {
	
		return mStuds;
	}

}
