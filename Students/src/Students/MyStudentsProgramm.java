package Students;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class MyStudentsProgramm {

	private final StudentsDataBase MyBase = new StudentsXmlBase("BaseStuds");
	final StudGUIInterface mGUI = new StudGUIInterface();
	
	public static void main(String[] args) {
		MyStudentsProgramm app = new MyStudentsProgramm();
		app.mGUI.Menu();
	}
	
	class StudGUIInterface{
		JFrame mainWindow;
		Container cpMW;
		JButton BAdd;
		JButton BRemove;
		JList<Student> ListStudents;
		JLabel info;
		JLabel LSeek;
		JButton BSeek;
		
		JLabel Lname;
		JLabel Lsurname;
		JLabel Lthirdname;
		JLabel Lbirth;
		JLabel Lgroup;
		JLabel Ldeportament;
		
		JTextField TFname;
		JTextField TFsurname;
		JTextField TFthirdname;
		JTextField TFbirth;
		JTextField TFgroup;
		JTextField TFdeportament;
		
		DefaultListModel<Student> listModel;
		
		public StudGUIInterface() {
			
			mainWindow = new JFrame();
			mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			cpMW = mainWindow.getContentPane();
			BAdd = new JButton("Add new student");
			BRemove = new JButton("Remove");
			BSeek = new JButton("Find Studs");
			info = new JLabel("Nice to meet you");
			LSeek = new JLabel("Поиск студентов:");
			
			listModel = new DefaultListModel<Student>();
			for(Student st:MyBase.GetAllStudents()){
				listModel.addElement(st);
			}
			
			ListStudents = new JList<Student>(listModel);
			
			Lname = new JLabel("Name:");
			Lsurname = new JLabel("Surname:");
			Lthirdname = new JLabel("ThirdName:");
			Lbirth= new JLabel("Birth:");
			Lgroup = new JLabel("group:");
			Ldeportament = new JLabel("Deportament");
			
			TFname = new JTextField(10);
			TFsurname = new JTextField(10);
			TFthirdname = new JTextField(10);
			TFbirth= new JTextField(10);
			TFgroup = new JTextField(4);
			TFdeportament = new JTextField(10);
			
			Panel PSeek = new Panel(new GridLayout(2, 1));
			PSeek.add(LSeek);
			PSeek.add(BSeek);
			
			Panel PName = new Panel(new GridLayout(2, 1));
			PName.add(Lname);
			PName.add(TFname);
			Panel PSurname = new Panel(new GridLayout(2, 1));
			PSurname.add(Lsurname);
			PSurname.add(TFsurname);
			Panel PThirdname = new Panel(new GridLayout(2, 1));
			PThirdname.add(Lthirdname);
			PThirdname.add(TFthirdname);
			Panel PBirth = new Panel(new GridLayout(2, 1));
			PBirth.add(Lbirth);
			PBirth.add(TFbirth);
			Panel PDeportament = new Panel(new GridLayout(2, 1));
			PDeportament.add(Ldeportament);
			PDeportament.add(TFdeportament);
			Panel PGroup = new Panel(new GridLayout(2, 1));
			PGroup.add(Lgroup);
			PGroup.add(TFgroup);
			
			
			
			Panel buttons = new Panel();
			buttons.setLayout(new GridLayout(5,1));
			buttons.add(BAdd);
			buttons.add(BRemove);
			
			Panel topSearthPanel = new Panel();
			topSearthPanel.setLayout(new FlowLayout());
			topSearthPanel.add(PSeek);
			topSearthPanel.add(PName);
			topSearthPanel.add(PSurname);
			topSearthPanel.add(PThirdname);
			topSearthPanel.add(PBirth);
			topSearthPanel.add(PDeportament);
			topSearthPanel.add(PGroup);
			
			cpMW.add(ListStudents,BorderLayout.CENTER);
			cpMW.add(buttons,BorderLayout.EAST);
			cpMW.add(info,BorderLayout.SOUTH);
			cpMW.add(topSearthPanel,BorderLayout.NORTH);
			InitListeners();
		}
		public void Menu(){
			mainWindow.pack();
			mainWindow.setVisible(true);
		}
		private void InitListeners(){
			BAdd.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					final CreateStudentDialog DialogNewStud = new CreateStudentDialog();
					DialogNewStud.pack();
					DialogNewStud.setVisible(true);
					DialogNewStud.getOkButton().addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							if(hasEmptyFields()){
								JOptionPane.showMessageDialog(mainWindow, "All fields must be filld", "has empty fields!", JOptionPane.WARNING_MESSAGE);
							}
							else{
								Student newStud = new Student(DialogNewStud.TFname.getText(),
										DialogNewStud.TFsurname.getText(),
										DialogNewStud.TFthirdname.getText(),
										DialogNewStud.TFdeportament.getText(),
										DialogNewStud.TFbirth.getText(),
										DialogNewStud.TFgroup.getText());
								MyBase.AddStudent(newStud);
								listModel.addElement(newStud);
								DialogNewStud.dispose();
							}
						}
						public boolean hasEmptyFields(){
							return (DialogNewStud.TFname.getText().isEmpty() ||
									DialogNewStud.TFsurname.getText().isEmpty() ||
									DialogNewStud.TFthirdname.getText().isEmpty() ||
									DialogNewStud.TFbirth.getText().isEmpty() ||
									DialogNewStud.TFdeportament.getText().isEmpty() ||
									DialogNewStud.TFgroup.getText().isEmpty());
						}
					});
					DialogNewStud.getCloseButton().addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent arg0) {
							DialogNewStud.dispose();
							
						}
					});
				}
			});
			
			BRemove.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {

					for(Student st :ListStudents.getSelectedValuesList()){
						MyBase.RemoveStudent(st);
						listModel.removeElement(st);
					}
//					for(Student st : MyBase.GetAllStudents()){
//						System.out.println(st);
//					}
				}
			});
			
			BSeek.addActionListener(new SeekControlListener());
			
		}
		
		class SeekControlListener implements ActionListener{
			
			private boolean isAllFieldsEmpty(){
				return (TFname.getText().isEmpty() &&
						TFsurname.getText().isEmpty() &&
						TFthirdname.getText().isEmpty() &&
						TFbirth.getText().isEmpty() &&
						TFdeportament.getText().isEmpty() &&
						TFgroup.getText().isEmpty());
			}
	
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if(isAllFieldsEmpty()){
					listModel.removeAllElements();
					for(Student st : MyBase.GetAllStudents()){
						listModel.addElement(st);
					}
				}
				else{
					StudentsFiltrator sf = new StudentsFiltrator();
					
					if(!TFname.getText().isEmpty()){
					StudentsFilter nameFilter = new StudentsFIOConFilter(TFname.getText(),null,null);
					sf.addFilter(nameFilter);
					}
					if(!TFsurname.getText().isEmpty()){
					StudentsFilter surnameFilter = new StudentsFIOConFilter(null,TFsurname.getText(),null);
					sf.addFilter(surnameFilter);
					}
					if(!TFthirdname.getText().isEmpty()){
					StudentsFilter thirdnameFilter = new StudentsFIOConFilter(null,null,TFthirdname.getText());
					sf.addFilter(thirdnameFilter);
					}
					if(!TFbirth.getText().isEmpty()){
					StudentsFilter birthFilter = new StudentsBirthFilter(TFbirth.getText());
					sf.addFilter(birthFilter);
					}
					if(!TFdeportament.getText().isEmpty()){
					StudentsFilter deportamentFilter = new StudentsDepartamentFilter(TFdeportament.getText());
					sf.addFilter(deportamentFilter);
					}
					if(!TFgroup.getText().isEmpty()){
					StudentsFilter groupFilter = new StudentsGroupFilter(TFgroup.getText());
					sf.addFilter(groupFilter);
					}

					listModel.removeAllElements();
					
					List<Student> newList = sf.getFilteredListFrom(MyBase.GetAllStudents());
					
					for(Student st : newList){
						listModel.addElement(st);
					}
				}
				
			}
		}
	}
	
	class StudConsoleInterface
	{
		final String FirstString = new String("Учёт студентов:\n" +
				"1___Создать нового студента\n" +
				"2___Удалить студента\n" +
				"3___Найти студента\n" +
				"4___Вывести список группы студентов\n" +
				"5___Поиск с использованием фильтра\n" +
				"6___Выход из приложения");

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		public void Menu()
		{
			///////////////////////////
			Student newStud = new Student("Владимир","Малышев","Николаевич","прог","290692","1");
			MyBase.AddStudent(newStud);
			Student newStud2 = new Student("Дмитрий","Бубнов","ХЗ","прог","ХЗ","1");
			MyBase.AddStudent(newStud2);
			Student newStud3 = new Student("Василий","Пупкин","Хренов","депутат","вне времени","777");
			MyBase.AddStudent(newStud3);
			Student newStud4 = new Student("ВАСЯ","Бубнов","ХЗ","прог","ХЗ","1");
			MyBase.AddStudent(newStud2);
			//
			ObjectOutputStream os;
			try{
			os = new ObjectOutputStream(new FileOutputStream(new File("student.obj")));
			os.writeObject(newStud);
			os.writeObject(newStud2);
			os.writeObject(newStud3);
			}
			catch(IOException ex){
				System.out.println("error");
			}
			ObjectInputStream ois;
			Student nstud;
			Student nstud1;
			Student nstud2;
			try{
				ois = new ObjectInputStream(new FileInputStream(new File("student.obj")));
				nstud = (Student)ois.readObject();
				nstud1 = (Student)ois.readObject();
				nstud2 = (Student)ois.readObject();
				System.out.println("сохраненные и извлеченные студенты : "+ nstud + nstud1 + nstud2);
				}
				catch(IOException ex){
					System.out.println("error");
				}
				catch(ClassNotFoundException exc){
					System.out.println("error read");
				}
			
			TreeSet<Student> mSet = new TreeSet<Student>();
			mSet.add(newStud);
			mSet.add(newStud2);
			mSet.add(newStud3);
			mSet.add(newStud4);
			for(Student st: mSet){
				System.out.println(st);
			}
			/////////////////
			boolean isExit = false;
			
			while(isExit == false)
			{
				System.out.println(FirstString);
				String input = new String();
			
				try{
					input = reader.readLine();
					
					switch(input)
					{
					case "1":
						CreateNewStudent();break;
					case "2":
						RemoveStudent();break;
					case "3":
						FindStudents();break;
					case "4":
						PrintGroupStudents();break;
					case "5":
						FindStudentsWithFilter();break;
					case "6":
						System.out.println("EXIT");
						isExit = true;
						break;
					default: System.out.println("Ошибка, не верная командда");
						isExit = false;
					}
					
				}
				catch(IOException ex){
					System.out.println("ошибка ввода");
					System.exit(1);
				}
			}

			
		}
		
		//methods return false if user want bask to menu
		public void CreateNewStudent()throws IOException{
			System.out.println("Вы можете ввести -1 в любе время для выхода в главное меню\n");
			String outArr[] = new String[]{"Введите имя студента:\n",
					"Введите фамилию студента:\n",
					"Введите отчесто студента:\n",
					"Введите дату рождения студента:\n",
					"Введите факультет студента:\n",
					"Введите группу студента:\n"};		
			
			String inArr[] = new String[6];

				for(int i = 0;i < outArr.length;i++){
					System.out.println(outArr[i]);
					inArr[i] = reader.readLine();
					if(inArr[i] == "-1") return;
				}
	
			Student newStud = new Student(inArr[0],inArr[1],inArr[2],inArr[3],inArr[4],inArr[5]);
			MyBase.AddStudent(newStud);
			
			System.out.println("Студент "+inArr[1]+" "+inArr[0]+" "+inArr[2]+" Дата рождения:"+inArr[3]+
					" Успешно добавлен на факультет:"+inArr[4]+" в группу:"+inArr[5]);
		}
		
		public void RemoveStudent()throws IOException{
			System.out.println("Удаление студента из базы:\n" +
					"Введите через enter <ФАМИЛИЯ> <ИМЯ> <ОТЧEСТВО>"+
					"(если вы не уверены то воспользуйтесь поиском)(-1 для выхода в меню)");
				
			String FIO[] = requestFIO();
			if(FIO == null) return;
			List<Student> findStuds = MyBase.FindStudents(FIO[0],FIO[1],FIO[2]);
			
			if(findStuds.isEmpty()){
				System.out.println("Нет таких студентов");
				return;
			}
			else if(findStuds.size() == 1){
				MyBase.RemoveStudent(findStuds.get(0));
				System.out.println("Студент успешно удален");
			}
		}
		
		public void FindStudents()throws IOException{
			System.out.println("поиск студентов:\n" +
					" введите фамилию имя и отчество для поиска,\n" +
					" все совпадения будут выведены ниже" +
					"(\n-1 для выхода в меню)");
			
			String FIO[] = requestFIO();
			if(FIO == null) return;
			List<Student> finded = MyBase.FindCoincidence(FIO[0], FIO[1], FIO[2]);
			
			if(finded.isEmpty())
			{
				System.out.println("Нет совпадений");
				return;
			}
			System.out.println("Найденные студенты:");
			PrintStudents(finded);
		}
		
		public void PrintGroupStudents()throws IOException{
			System.out.println("Введите номер группы для печати(-1 для выхода в меню)");
			
				String group = reader.readLine();
				List<Student> studs = MyBase.FindStudentsGroup(group);
				
				if(studs.isEmpty()){
					System.out.println("нет студентов в этой группе");
					return;
				}
				PrintStudents(studs);
		}
		
		public void PrintStudents(List<Student> studs){
			
			for(Student stud : studs){
			System.out.println(stud.getSecondName() +" "+ stud.getFirstName()+" "+stud.getThirdName());
			}
		}
		
		private String[] requestFIO()throws IOException{
			String outArr[] = new String[]{"Введите имя студента:\n",
					"Введите фамилию студента:\n",
					"Введите отчесто студента:\n"};
	
			String inArr[] = new String[3];

				for(int i = 0;i < outArr.length;i++){
					System.out.println(outArr[i]);
					inArr[i] = reader.readLine();
					if(inArr[i] == "-1") return null;
				}

			return inArr;
		}
	
		@SuppressWarnings("unchecked")
		public void FindStudentsWithFilter()throws IOException{
			System.out.println("Поиск студентов по фильтру\n" +
					"1) найти совпадения по ФИО\n" +
					"2) найти совпадения по дате рождения \n" +
					"3) найти совпадения по группе студента");
			
			@SuppressWarnings("rawtypes")
			List finded = null;
			switch(reader.readLine()){
			case"1": String FIO[] = requestFIO();
				finded = MyBase.FindFromFilter(new StudentsFIOConFilter(FIO[0], FIO[1], FIO[2]));
				break;
			case"2":System.out.println("Введие пожалуйста дату рождения:");
				finded = MyBase.FindFromFilter(
						new StudentsBirthFilter(
								reader.readLine()));
				break;
			case"3":System.out.println("Введие пожалуйста группу для поиска:");
			finded = MyBase.FindFromFilter(
					new StudentsGroupFilter(
							reader.readLine()));
			break;
			}
			if(finded == null || finded.isEmpty())
				System.out.println("нет таких студентов");
			else
			PrintStudents((ArrayList<Student>)finded);
		}
		
	}

}
