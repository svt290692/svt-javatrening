package Students;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class StudentsFiltrator {
	final private List<StudentsFilter> filters = new ArrayList<StudentsFilter>(20);
	public StudentsFiltrator(StudentsFilter... filters) {
		for(StudentsFilter sf : filters){
			this.filters.add(sf);
		}
	}
	public StudentsFiltrator(){
	}
	public void addFilter(StudentsFilter sf){
		filters.add(sf);
	}
	public List<Student> getFilteredListFrom(Collection< ? extends Student> col){
		List<Student> studs = new ArrayList<Student>();
		for(Student st : col){
			
			if(checkValid(st))
				studs.add(st);
		}
		return studs;
	}
	private boolean checkValid(Student st){
		
		for(StudentsFilter filter : filters){
			if(!filter.isValid(st))
				return false;
		}
		return true;
	}
}