package Students;

import java.util.ArrayList;
import java.util.List;

public class StudentsArrayBase implements StudentsDataBase {

	private ArrayList<Student> mStuds;
	StudentsArrayBase()
	{
		mStuds = new ArrayList<Student>();
	}
	@Override
	public void AddStudent(Student stud) {
		if(stud == null){
			System.out.println("ERROR NULL POINTER");
			return;
		}
		mStuds.add(stud);
	}

	@Override
	public boolean RemoveStudent(Student stud) {
		for(Student st : mStuds)
		{
			if(st == stud)
			{
				mStuds.remove(st);
				return true;
			}
		}
		return false;
	}

	@Override
	public ArrayList<Student> FindStudents(String Sname) {
		
		ArrayList<Student> findStuds = new ArrayList<Student>();
		for(Student st : mStuds)
		{
			if(st.getSecondName().equals(Sname)){
				findStuds.add(st);
			}
		}
		return findStuds;
	}

	@Override
	public ArrayList<Student> FindStudents(String fName, String sName,
			String tName) {
		ArrayList<Student> findStuds = new ArrayList<Student>();
		
		for(Student st : mStuds)
		{
			if(st.getFirstName().equals(fName)  && st.getSecondName().equals(sName)  &&
					st.getThirdName().equals(tName) )
			{
				findStuds.add(st);
			}
		}
		return findStuds;
	}

	@Override
	public ArrayList<Student> FindStudentsGroup(String group) {
		ArrayList<Student> findStuds = new ArrayList<Student>();
		
		for(Student st : mStuds)
		{
			if(st.getGroup().equals(group))
			{
				findStuds.add(st);
			}
		}
		return findStuds;
	}

	@Override
	public ArrayList<Student> FindCoincidence(String fName, String sName,
			String tName){
		ArrayList<Student> findStuds = new ArrayList<Student>();
		
		for(Student st : mStuds)
		{
			if(st.getFirstName().equals(fName) ||
					st.getSecondName().equals(sName) ||
					st.getThirdName().equals(tName))
				findStuds.add(st);
		}
		return findStuds;
	}
	@Override
	public List<Student> FindFromFilter(StudentsFilter filter) {
		
		List<Student> finded = new ArrayList<Student>();
		for(Student st : mStuds){
			if(filter.isValid(st))
				finded.add(st);
		}
		return finded;
	}
	@Override
	public List<Student> GetAllStudents() {
		return mStuds;
	}
}
