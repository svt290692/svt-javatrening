package Students;

public class StudentsFIOConFilter implements StudentsFilter {
	
	final String fName;
	final String sName;
	final String tName;
	
	public StudentsFIOConFilter(String fName, String sName, String tName) {
		super();
		this.fName = fName;
		this.sName = sName;
		this.tName = tName;
	}
	@Override
	public boolean isValid(Student stud) {
		if(		stud.getFirstName().equals(fName) ||
				stud.getSecondName().equals(sName)||
				stud.getThirdName().equals(tName))
		return true;
		else
			return false;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fName == null) ? 0 : fName.hashCode());
		result = prime * result + ((sName == null) ? 0 : sName.hashCode());
		result = prime * result + ((tName == null) ? 0 : tName.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof StudentsFIOConFilter)) {
			return false;
		}
		StudentsFIOConFilter other = (StudentsFIOConFilter) obj;
		if (fName == null) {
			if (other.fName != null) {
				return false;
			}
		} else if (!fName.equals(other.fName)) {
			return false;
		}
		if (sName == null) {
			if (other.sName != null) {
				return false;
			}
		} else if (!sName.equals(other.sName)) {
			return false;
		}
		if (tName == null) {
			if (other.tName != null) {
				return false;
			}
		} else if (!tName.equals(other.tName)) {
			return false;
		}
		return true;
	}

	

}
