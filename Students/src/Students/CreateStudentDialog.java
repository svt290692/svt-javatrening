package Students;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Panel;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class CreateStudentDialog extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1456789876543L;
	final JButton Bok;
	final JButton Bclose;
	
	final JTextField TFname;
	final JTextField TFsurname;
	final JTextField TFthirdname;
	final JTextField TFbirth;
	final JTextField TFgroup;
	final JTextField TFdeportament;
	
	final JLabel Lname;
	final JLabel Lsurname;
	final JLabel Lthirdname;
	final JLabel Lbirth;
	final JLabel Lgroup;
	final JLabel Ldeportament;
	public CreateStudentDialog() {
		super();
		Lname = new JLabel("Name:");
		Lsurname = new JLabel("Surname:");
		Lthirdname = new JLabel("ThirdName:");
		Lbirth= new JLabel("Birth:");
		Lgroup = new JLabel("group:");
		Ldeportament = new JLabel("Deportament");
		
		TFname = new JTextField(10);
		TFsurname = new JTextField(10);
		TFthirdname = new JTextField(10);
		TFbirth= new JTextField(10);
		TFgroup = new JTextField(4);
		TFdeportament = new JTextField(10);
		
		Panel PName = new Panel(new GridLayout(2, 1));
		PName.add(Lname);
		PName.add(TFname);
		Panel PSurname = new Panel(new GridLayout(2, 1));
		PSurname.add(Lsurname);
		PSurname.add(TFsurname);
		Panel PThirdname = new Panel(new GridLayout(2, 1));
		PThirdname.add(Lthirdname);
		PThirdname.add(TFthirdname);
		Panel PBirth = new Panel(new GridLayout(2, 1));
		PBirth.add(Lbirth);
		PBirth.add(TFbirth);
		Panel PDeportament = new Panel(new GridLayout(2, 1));
		PDeportament.add(Ldeportament);
		PDeportament.add(TFdeportament);
		Panel PGroup = new Panel(new GridLayout(2, 1));
		PGroup.add(Lgroup);
		PGroup.add(TFgroup);
		
		Panel fields = new Panel(new FlowLayout());
		fields.add(PName);
		fields.add(PSurname);
		fields.add(PThirdname);
		fields.add(PBirth);
		fields.add(PDeportament);
		fields.add(PGroup);
		
		Bok = new JButton("Ok");
		Bclose = new JButton("Close");
		
		Panel buttons = new Panel(new FlowLayout());
		buttons.add(Bok);
		buttons.add(Bclose);
		
		setLayout(new BorderLayout());
		add(fields,BorderLayout.CENTER);
		add(new JLabel("Add new Student:"),BorderLayout.NORTH);
		add(buttons,BorderLayout.SOUTH);
	}
	public JButton getOkButton(){
		return Bok;
	}
	public JButton getCloseButton(){
		return Bclose;
	}
}
