package Students;

public class StudentsFIOExactMathingFilter implements StudentsFilter {

	final String fName;
	final String sName;
	final String tName;
	
	public StudentsFIOExactMathingFilter(String fName, String sName,
			String tName) {
		super();
		this.fName = fName;
		this.sName = sName;
		this.tName = tName;
	}

	@Override
	public boolean isValid(Student stud) {
		if(		stud.getFirstName().equals(fName) &&
				stud.getSecondName().equals(sName)&&
				stud.getThirdName().equals(tName))
		return true;
		else
			return false;
	}

}
