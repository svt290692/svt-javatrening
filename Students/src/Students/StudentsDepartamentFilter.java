package Students;

public class StudentsDepartamentFilter implements StudentsFilter {

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((deportament == null) ? 0 : deportament.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentsDepartamentFilter other = (StudentsDepartamentFilter) obj;
		if (deportament == null) {
			if (other.deportament != null)
				return false;
		} else if (!deportament.equals(other.deportament))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "StudentsDepartamentFilter [deportament=" + deportament + "]";
	}
	public StudentsDepartamentFilter(String deportament) {
		super();
		this.deportament = deportament;
	}
	String deportament;
	@Override
	public boolean isValid(Student stud) {
		return stud.getDeportament().equals(deportament);
	}

}
