package Students;
import java.util.List;

interface StudentsDataBase {
	
	public void AddStudent(Student stud);
	public boolean RemoveStudent(Student stud);
	public List<Student> FindStudents(String fName,String sName,String tName);
	public List<Student> FindStudents(String sName);
	public List<Student> FindStudentsGroup(String group);
	public List<Student> FindCoincidence(String fName,String sName,String tName);
	public List<Student> FindFromFilter(StudentsFilter filter);
	public List<Student> GetAllStudents();
	
}
