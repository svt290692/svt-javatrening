package Students;

public class StudentsBirthFilter implements StudentsFilter {
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((Birth == null) ? 0 : Birth.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentsBirthFilter other = (StudentsBirthFilter) obj;
		if (Birth == null) {
			if (other.Birth != null)
				return false;
		} else if (!Birth.equals(other.Birth))
			return false;
		return true;
	}
	public StudentsBirthFilter(String birth) {
		super();
		Birth = birth;
	}
	String Birth;
	@Override
	public boolean isValid(Student stud) {
		if(stud.getBirth().equals(Birth))
		return true;
		else
			return false;
	}

}
