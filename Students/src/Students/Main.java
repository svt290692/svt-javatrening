package Students;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Main {

	private StudentsDataBase MyBase = new StudentsArrayBase();;
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	final StudInterface mUI = new StudInterface();
	
	public static void main(String[] args) {
		Main app = new Main();
		app.mUI.Menu();
	}
	
	class StudInterface
	{
		final String FirstString = new String("Учёт студентов:\n" +
				"1___Создать нового студента\n" +
				"2___Удалить студента\n" +
				"3___Найти студента\n" +
				"4___Вывести список группы студентов\n" +
				"5___Поиск с использованием фильтра\n" +
				"6___Выход из приложения");
		public void Menu()
		{
			///////////////////////////
			Student newStud = new Student("Владимир","Малышев","Николаевич","прог","290692","1");
			MyBase.AddStudent(newStud);
			Student newStud2 = new Student("Дмитрий","Бубнов","ХЗ","прог","ХЗ","1");
			MyBase.AddStudent(newStud2);
			Student newStud3 = new Student("Василий","Пупкин","Хренов","депутат","вне времени","777");
			MyBase.AddStudent(newStud3);
			Student newStud4 = new Student("ВАСЯ","Бубнов","ХЗ","прог","ХЗ","1");
			MyBase.AddStudent(newStud2);
			//
			ObjectOutputStream os;
			try{
			os = new ObjectOutputStream(new FileOutputStream(new File("student.obj")));
			os.writeObject(newStud);
			os.writeObject(newStud2);
			os.writeObject(newStud3);
			}
			catch(IOException ex){
				System.out.println("error");
			}
			ObjectInputStream ois;
			Student nstud;
			Student nstud1;
			Student nstud2;
			try{
				ois = new ObjectInputStream(new FileInputStream(new File("student.obj")));
				nstud = (Student)ois.readObject();
				nstud1 = (Student)ois.readObject();
				nstud2 = (Student)ois.readObject();
				System.out.println("сохраненные и извлеченные студенты : "+ nstud + nstud1 + nstud2);
				}
				catch(IOException ex){
					System.out.println("error");
				}
				catch(ClassNotFoundException exc){
					System.out.println("error read");
				}
			
			TreeSet<Student> mSet = new TreeSet<Student>();
			mSet.add(newStud);
			mSet.add(newStud2);
			mSet.add(newStud3);
			mSet.add(newStud4);
			for(Student st: mSet){
				System.out.println(st);
			}
			/////////////////
			boolean isExit = false;
			
			while(isExit == false)
			{
				System.out.println(FirstString);
				String input = new String();
			
				try{
					input = reader.readLine();
					
					switch(input)
					{
					case "1":
						CreateNewStudent();break;
					case "2":
						RemoveStudent();break;
					case "3":
						FindStudents();break;
					case "4":
						PrintGroupStudents();break;
					case "5":
						FindStudentsWithFilter();break;
					case "6":
						System.out.println("EXIT");
						isExit = true;
						break;
					default: System.out.println("Ошибка, не верная командда");
						isExit = false;
					}
					
				}
				catch(IOException ex){
					System.out.println("ошибка ввода");
					System.exit(1);
				}
			}

			
		}
		
		//methods return false if user want bask to menu
		public void CreateNewStudent()throws IOException{
			System.out.println("Вы можете ввести -1 в любе время для выхода в главное меню\n");
			String outArr[] = new String[]{"Введите имя студента:\n",
					"Введите фамилию студента:\n",
					"Введите отчесто студента:\n",
					"Введите дату рождения студента:\n",
					"Введите факультет студента:\n",
					"Введите группу студента:\n"};		
			
			String inArr[] = new String[6];

				for(int i = 0;i < outArr.length;i++){
					System.out.println(outArr[i]);
					inArr[i] = reader.readLine();
					if(inArr[i] == "-1") return;
				}
	
			Student newStud = new Student(inArr[0],inArr[1],inArr[2],inArr[3],inArr[4],inArr[5]);
			MyBase.AddStudent(newStud);
			
			System.out.println("Студент "+inArr[1]+" "+inArr[0]+" "+inArr[2]+" Дата рождения:"+inArr[3]+
					" Успешно добавлен на факультет:"+inArr[4]+" в группу:"+inArr[5]);
		}
		
		public void RemoveStudent()throws IOException{
			System.out.println("Удаление студента из базы:\n" +
					"Введите через enter <ФАМИЛИЯ> <ИМЯ> <ОТЧEСТВО>"+
					"(если вы не уверены то воспользуйтесь поиском)(-1 для выхода в меню)");
				
			String FIO[] = requestFIO();
			if(FIO == null) return;
			ArrayList<Student> findStuds = MyBase.FindStudents(FIO[0],FIO[1],FIO[2]);
			
			if(findStuds.isEmpty()){
				System.out.println("Нет таких студентов");
				return;
			}
			else if(findStuds.size() == 1){
				MyBase.RemoveStudent(findStuds.get(0));
				System.out.println("Студент успешно удален");
			}
		}
		
		public void FindStudents()throws IOException{
			System.out.println("поиск студентов:\n" +
					" введите фамилию имя и отчество для поиска,\n" +
					" все совпадения будут выведены ниже" +
					"(\n-1 для выхода в меню)");
			
			String FIO[] = requestFIO();
			if(FIO == null) return;
			ArrayList<Student> finded = MyBase.FindCoincidence(FIO[0], FIO[1], FIO[2]);
			
			if(finded.isEmpty())
			{
				System.out.println("Нет совпадений");
				return;
			}
			System.out.println("Найденные студенты:");
			PrintStudents(finded);
		}
		
		public void PrintGroupStudents()throws IOException{
			System.out.println("Введите номер группы для печати(-1 для выхода в меню)");
			
				String group = reader.readLine();
				ArrayList<Student> studs = MyBase.FindStudentsGroup(group);
				
				if(studs.isEmpty()){
					System.out.println("нет студентов в этой группе");
					return;
				}
				PrintStudents(studs);
		}
		
		public void PrintStudents(ArrayList<Student> studs){
			
			for(Student stud : studs){
			System.out.println(stud.getSecondName() +" "+ stud.getFirstName()+" "+stud.getThirdName());
			}
		}
		
		private String[] requestFIO()throws IOException{
			String outArr[] = new String[]{"Введите имя студента:\n",
					"Введите фамилию студента:\n",
					"Введите отчесто студента:\n"};
	
			String inArr[] = new String[3];

				for(int i = 0;i < outArr.length;i++){
					System.out.println(outArr[i]);
					inArr[i] = reader.readLine();
					if(inArr[i] == "-1") return null;
				}

			return inArr;
		}
	
		public void FindStudentsWithFilter()throws IOException{
			System.out.println("Поиск студентов по фильтру\n" +
					"1) найти совпадения по ФИО\n" +
					"2) найти совпадения по дате рождения \n" +
					"3) найти совпадения по группе студента");
			
			List finded = null;
			switch(reader.readLine()){
			case"1": String FIO[] = requestFIO();
				finded = MyBase.FindFromFilter(new StudentsFIOConFilter(FIO[0], FIO[1], FIO[2]));
				break;
			case"2":System.out.println("Введие пожалуйста дату рождения:");
				finded = MyBase.FindFromFilter(
						new StudentsBirthFilter(
								reader.readLine()));
				break;
			case"3":System.out.println("Введие пожалуйста группу для поиска:");
			finded = MyBase.FindFromFilter(
					new StudentsGroupFilter(
							reader.readLine()));
			break;
			}
			if(finded == null || finded.isEmpty())
				System.out.println("нет таких студентов");
			else
			PrintStudents((ArrayList<Student>)finded);
		}
		
	}
}
